<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Filter\SxtReplaceMwHeaderFilter.
 */

namespace Drupal\sxt_mediawiki\Plugin\Filter;

use Drupal\sxt_mediawiki\SxtMediawiki;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter for replacing headers in MediaWiki text.
 * 
 * @Filter(
 *   id = "sxt_replace_mw_header",
 *   title = @Translation("Replace MediaWiki header"),
 *   description = @Translation("Headers are replaced where they must not be supported, i.g. for comments in MediaWiki format."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class SxtReplaceMwHeaderFilter extends FilterBase {


  /**
   * {@inheritdoc}
   */
  public function process($mw_text, $langcode) {
    $new_text = SxtMediawiki::replaceMwHeader($mw_text);
    return new FilterProcessResult($new_text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return t('NOTE: wiki header are not supported and will be replaced.');
  }

}
