<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Controller\SxtMwCommentController.
 */

namespace Drupal\sxt_mediawiki\Controller;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\comment\Controller\CommentController;

/**
 * Controller for the comment entity with mwnodecomment.manager and mwcomment.reply route.
 */
class SxtMwCommentController extends CommentController {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('http_kernel'), //
        $container->get('sxt_mediawiki.mwnodecomment.manager'), // use own manager
        $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function redirectNode(EntityInterface $node) {
    $fields = $this->commentManager->getFields('node');
    // Legacy nodes only had a single comment field, so use the first comment
    // field on the entity.
    if (!empty($fields) && ($field_names = array_keys($fields)) && ($field_name = reset($field_names))) {
      $sxt_mwroute = 'sxt_mediawiki.mwcomment.reply';
      return $this->redirect($sxt_mwroute, [
            'entity_type' => 'node',
            'entity' => $node->id(),
            'field_name' => $field_name,
      ]);
    }
    throw new NotFoundHttpException();
  }

  /**
   * {@inheritdoc}
   */
  public function getReplyForm(Request $request, EntityInterface $entity, $field_name, $pid = NULL) {
    $build = parent::getReplyForm($request, $entity, $field_name, $pid);

    $comment_form = &$build['comment_form'];
    $action = $comment_form['#action'];

    $search = '/comment/reply/node/';
    $search_pos = strpos($action, $search);
    $mwcomment_pos = strpos($action, 'mwcomment');
    if ($search_pos !== FALSE && $mwcomment_pos !== FALSE && $mwcomment_pos > $search_pos) {
      $replace = '/mwcomment/reply/node/';
      $new_url = str_replace($search, $replace, $action);
      $comment_form['#action'] = $new_url;
      $comment_form['#attached']['drupalSettings']['ajaxTrustedUrl'] = [$new_url => TRUE];
    }

    return $build;
  }

}
