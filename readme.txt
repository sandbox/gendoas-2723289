// Step 1: Install/Konfigure MediaWiki Server
- Versin 2.9 or greater
- Extension OAuth (https://www.mediawiki.org/wiki/Extension:OAuth)
- Create Consumer (Special:Special Pages)
    !! OAuth-Callback-URL: oob
    !! public RSA-key/CERTIFICATE: ... (to generate by openssl)
    !! consumer key/consumer secret
- LocalSettings.php
    wfLoadExtension( 'OAuth' );
    $wgGroupPermissions['sysop']['mwoauthproposeconsumer'] = true;
    $wgGroupPermissions['sysop']['mwoauthupdateownconsumer'] = true;
    $wgGroupPermissions['sysop']['mwoauthmanageconsumer'] = true;
    $wgGroupPermissions['sysop']['mwoauthsuppress'] = true;
    $wgGroupPermissions['sysop']['mwoauthviewsuppressed'] = true;
    $wgGroupPermissions['sysop']['mwoauthviewprivate'] = true;
    $wgGroupPermissions['sysop']['mwoauthmanagemygrants'] = true;
    if (!$wgWhitelistRead) { $wgWhitelistRead = array(); }
    $wgWhitelistRead[] = 'Spezial:OAuth';


// Step 2: Drupal module sxt_mediawiki
- Install/activate sxt_mediawiki
- Settings (/admin/config/content/formats, mediawiki_format)
    - enter consumer key/consumer secret, save
- Settings again
    - url for authorizing (shown in the form when consumer exists and access not)
    - new browser tab with that url (do it immediately, request token expires!)
    - Login -> Dialog -> Allow (ignore message; acces token is created now)
- Open database
    - table oauth_accepted_consumer
    - get access token/access secret
- Settings again
    - enter access token/access secret, save

// Step 3: RsaSha1 files
- For security reasons set the path in settings, not reachable for users
- Upload the rsa files to that path as cert.txt and key.txt


// Step 4: Testing
- Create content with a mediawiki_format field 
- Test: view that content, .....ok? 

****    READY    ****

********************************************************************************
Problem: Authorization
- local ok, on life systen Authorization not got on mediawiki server.

Lokal:
- PHP Version 7.0.21-1~ubuntu16.04.1+deb.sury.org+1
- /etc/php/7.0/apache2/php.ini

Life:
- PHP Version 7.0.18-0ubuntu0.16.04.1
- /etc/php/7.0/cgi/php.ini 


Workaround:
\Drupal\sxt_mediawiki\SxtOAuth\SxtOAuthClient::makeOAuthCall()
    $headers = $req->toHeader();
    $headers = str_replace('_', '', $headers);
    $headers = str_replace('Authorization:', 'Myauthorization:', $headers);
    return $this->makeCurlCall($url, $headers, $isPost, $postFields, $this->config);

!!!!!!   mediawiki hacked   !!!!!!
mediawiki/include/WebRequest::initHeaders()
    if (isset($apacheHeaders['Myauthorization'])) {
        $apacheHeaders['Authorization'] = $apacheHeaders['Myauthorization'];
        unset($apacheHeaders['Myauthorization']);
        //wfDebugLog( 'xx-debug', 'initHeaders.Myauthorization: ' . json_encode($apacheHeaders));
    }

********************************************************************************