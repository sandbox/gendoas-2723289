<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\MwNodeCommentManager.
 */

namespace Drupal\sxt_mediawiki;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\comment\CommentManager;

/**
 * Comment manager contains common functions to manage Sxt MediaWiki comment fields.
 */
class MwNodeCommentManager extends CommentManager {

  /**
   * {@inheritdoc}
   */
  public function getFields($entity_type_id) {
    $entity_type = $this->entityManager->getDefinition($entity_type_id);
    if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
      return [];
    }

    $map = $this->entityManager->getFieldMapByFieldType('mwnodecomment_fieldtype');
    return isset($map[$entity_type_id]) ? $map[$entity_type_id] : [];
  }

}
