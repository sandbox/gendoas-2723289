<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\SxtOAuth\SxtOAuthClient.
 */

namespace Drupal\sxt_mediawiki\SxtOAuth;

use Drupal\sxt_oauthclient\Client;
use Drupal\sxt_oauthclient\Request;
use Drupal\sxt_oauthclient\Token;
use Drupal\sxt_oauthclient\Exception;

/**
 * Description of SxtOAuthClient
 *
 * @author albin
 */
class SxtOAuthClient extends Client {

  /**
   * SxtMwFilter settings: filter_mw_url, ...
   *
   * @var array 
   */
  protected $settings;

  /**
   * 
   * @param array $settings
   */
  public function setSettings($settings) {
    $this->settings = $settings;
  }

  public function apiParseText($text) {
    $aToken = $this->settings['filter_mw_atoken'];
    $aSecret = $this->settings['filter_mw_asecret'];
    $accessToken = new Token($aToken, $aSecret);

//    $prepend = "__NOTOC__ __NOEDITSECTION__"; //???
    $prepend = "__NOEDITSECTION__";
    $wikitext = "$prepend\n$text";
    $apiParams = [
      'action' => 'parse',
      'contentmodel' => 'wikitext',
      'text' => $wikitext,
      'format' => 'json',
    ];
    $this->setExtraParams($apiParams); // sign these too
    $url = $this->settings['filter_mw_url'] . '/api.php';
    $data = $this->makeOAuthCall($accessToken, $url, true, $apiParams);

    $filtered = FALSE;
    if ($data = $this->handleServerResponse($data)) {
      if (isset($data->parse) && isset($data->parse->text)) {
        $array = (array) $data->parse->text;
        if (!empty($array['*'])) {
          $text = $array['*'];
          $filtered = TRUE;
        }
      }
    }

    if (!$filtered) {
      $this->logger->error('apiParseText: not parsed.');
    }

    return $text;
  }

  /**
   * Handle server response.
   * 
   * @param string $data as json
   * @return json_decoded | FALSE
   *  Return json decoded data or FALSE on error
   */
  private function handleServerResponse($data) {
    $return = json_decode($data);
    if ($return === null) {
      $this->logger->error('Failed to decode server response as JSON: {response}', ['response' => $data]);
      return FALSE;
    }
    if (property_exists($return, 'error')) {
      $msg = json_encode($return->error);
      $this->logger->error('OAuth server error: {msg}', ['msg' => $msg]);
      return FALSE;
    }
    if (property_exists($return, 'warnings') && property_exists($return->warnings, 'parse')) {
      $msg = json_encode($return->warnings->parse);
      $this->logger->warning('OAuth server warning: {msg}', ['msg' => $msg]);
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   * 
   * Override. 
   * - Call makeOAuthCall with $isPost=TRUE
   */
  public function initiate() {
    $initUrl = $this->config->endpointURL .
        '/initiate&format=json&oauth_callback=' .
        urlencode($this->callbackUrl);
    $data = $this->makeOAuthCall(null, $initUrl, true);
    $return = json_decode($data);
    if ($return === null) {
      $this->logger->error('Failed to decode server response as JSON: {response}', ['response' => $data]);
      throw new Exception('Decoding server response failed.');
    }
    if (property_exists($return, 'error')) {
      $this->logger->error('OAuth server error {error}: {msg}', ['error' => $return->error, 'msg' => $return->message]);
      throw new Exception($return->message);
    }
    if (!property_exists($return, 'oauth_callback_confirmed') ||
        $return->oauth_callback_confirmed !== 'true'
    ) {
      throw new Exception("Callback wasn't confirmed");
    }
    $requestToken = new Token($return->key, $return->secret);
    $url = $this->config->redirURL ?:
        $this->config->endpointURL . "/authorize&";
    $url .= "oauth_token={$requestToken->key}&oauth_consumer_key={$this->config->consumer->key}";
    return [$url, $requestToken];
  }

  /**
   * {@inheritdoc}
   * 
   * Override. 
   *  - Use RsaSha1 instead of HmacSha1
   *  - For $isPost set $postFields from request object
   */
  public function makeOAuthCall(/* Token */ $token, $url, $isPost = false, array $postFields = null) {
    $params = [];
    // Get any params from the url
    if (strpos($url, '?')) {
      $parsed = parse_url($url);
      parse_str($parsed['query'], $params);
    }
    $params += $this->extraParams;
    if ($isPost && $postFields) {
      $params += $postFields;
    }
    $method = $isPost ? 'POST' : 'GET';
    $req = Request::fromConsumerAndToken($this->config->consumer, $token, $method, $url, $params);

    $rsaSha1 = new SxtOAuthRsaSha1($this->settings['filter_mw_rsapath']);
    $rsaSha1->logger = $this->logger;
    $req->signRequest($rsaSha1, $this->config->consumer, $token);

    if ($isPost) {
      $postFields = $req->getParameters();
    }
    $this->lastNonce = $req->getParameter('oauth_nonce');

    $headers = $req->toHeader();
    $headers = str_replace('_', '', $headers);
    $headers = str_replace('Authorization:', 'Myauthorization:', $headers);
    return $this->makeCurlCall($url, $headers, $isPost, $postFields, $this->config);
  }

}
