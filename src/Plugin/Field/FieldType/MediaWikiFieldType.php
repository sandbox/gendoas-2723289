<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldType\MediaWikiFieldType.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldType;

use Drupal\text\Plugin\Field\FieldType\TextLongItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Component\Utility\Random;

/**
 * Plugin implementation of the 'sxt_mediawiki' field type.
 *
 * @FieldType(
 *   id = "sxt_mediawiki",
 *   label = @Translation("MediaWiki"),
 *   description = @Translation("...............MediaWiki Field Type."),
 *   category = @Translation("Text"),
 *   default_widget = "sxt_mediawiki",
 *   default_formatter = "sxt_mediawiki"
 * )
 */
class MediaWikiFieldType extends TextLongItem {

  static $random = NULL;
  static $table_done = [];

  const PARA_TEXT = 'text';
  const PARA_ULIST = 'unorderedList';
  const PARA_OLIST = 'orderedList';
  const PARA_DEFLIST = 'definitionList';
  const PARA_TABLE = 'table';


  /**
   * {@inheritdoc}
   */
  public function instanceSettingsForm(array $form, FormStateInterface $form_state) {
    return ['text_processing' => ['#type' => 'value', '#value' => 1]];
  }

  protected static function random() {
    if (!self::$random) {
      self::$random = new Random();
    }
    return self::$random;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    self::$table_done = [];
//    $random = self::random();
    $settings = $field_definition->getSettings();
    $mw_text = '';

    $max_sections = isset($settings['max_sections']) ? $settings['max_sections'] : 5;
    $sections = mt_rand(2, $max_sections);
    for ($i = 0; $i < $sections; $i++) {
      $mw_text .= self::generateSection($settings, TRUE);
    }

    $values['value'] = $mw_text;
    return $values;
  }

  protected static function generateSection(array $settings, $top_level) {
    $mw_text = self::generateTitle($top_level);

    $max_subsections = isset($settings['max_subsections']) ? $settings['max_subsections'] : 3;
    $subsections = $top_level ? mt_rand(1, $max_subsections) : FALSE;

    if ($subsections) {
      $mw_text .= self::generateParagraphs(1, self::PARA_TEXT);
      $mw_text .= self::generateSection($settings, FALSE);
      $mw_text .= self::generateParagraphs(mt_rand(0, 2));
    }
    else {
      $mw_text .= self::generateParagraphs(mt_rand(1, 3));
    }
    return $mw_text;
  }

  protected static function generateTitle($top_level) {
    $title = self::generateTextLine();
    $h_prev = $top_level ? '== ' : '===';
    $h_post = $top_level ? ' ==' : ' ===';
    $title = $h_prev . $title . "$h_post\n";
    return $title;
  }

  protected static function getParagraphsAvailable() {
    // multiple items for weight, do not delete !!!
    return [
      self::PARA_TEXT,
      self::PARA_TEXT,
      self::PARA_TEXT,
      self::PARA_TEXT,
      self::PARA_TEXT,
      self::PARA_ULIST,
      self::PARA_ULIST,
      self::PARA_OLIST,
      self::PARA_DEFLIST,
      self::PARA_TABLE,
      self::PARA_TABLE,
      self::PARA_TABLE,
    ];
  }

  protected static function generateParagraphs($number, $first = NULL) {
    $available = static::getParagraphsAvailable();
    $paras = [];
    if ($first && in_array($first, $available)) {
      $paras[] = $first;
    }
    $available_last = count($available) - 1;
    while (count($paras) < $number) {
      $paras[] = $available[mt_rand(0, $available_last)];
    }


    $mw_text = '';
    foreach ($paras as $para) {
      $func = 'generate' . ucfirst($para);
      $mw_text .= self::{$func}();
    }

    return $mw_text;
  }

  protected static function generateText() {
    return self::random()->paragraphs(1);
  }

  protected static function generateUnorderedList() {
    return self::generateList(FALSE);
  }

  protected static function generateOrderedList() {
    return self::generateList(TRUE);
  }

  protected static function generateDefinitionList() {
    $mw_text = '';
    for ($i = 0; $i < mt_rand(3, 6); $i++) {
      $word = self::generateTextLine(1, 1, 3);
      $text = self::generateTextLine();
      $mw_text .= "; $word: $text\n";
    }

    return "$mw_text\n";
  }

  protected static function generateList($ordered = FALSE) {
    $list_char = $ordered ? '#' : '*';
    $mw_text = '';
    for ($i = 0; $i < mt_rand(3, 6); $i++) {
      $text = self::generateTextLine(1.8);
      $mw_text .= "$list_char $text\n";
    }

    return "$mw_text\n";
  }

  protected static function generateTextLine($factor = 1, $min = 3, $max = 6) {
    $random = self::random();
    $min = (integer) ($min * $factor);
    $max = (integer) ($max * $factor);
    $word_number = mt_rand($min, $max);

    $text = $random->sentences($word_number, FALSE);
    $text = str_replace('.', '', $text);
    $text = ucfirst(strtolower($text));
    $parts = explode(' ', $text);
    if (count($parts) > $word_number) {
      $parts = array_slice($parts, 0, $word_number);
      $text = implode(' ', $parts);
    }
    return $text;
  }

  protected static function generateTable() {
    $t1 = <<<EOT
{| class="wikitable" style="max-width: 480px;"
!colspan="6"|Shopping List
|-
|rowspan="2"|Bread & Butter
|Pie
|Buns
|Danish
|colspan="2"|Croissant
|-
|Cheese
|colspan="2"|Ice cream
|Butter
|Yogurt
|}
EOT;

    $t2 = <<<EOT
{| class="wikitable" style="max-width: 360px;"
|-
! scope="col"| Item
! scope="col"| Quantity
! scope="col"| Price
|-
! scope="row"| Bread
| 0.3 kg
| $0.65
|-
! scope="row"| Butter
| 0.125 kg
| $1.25
|-
! scope="row" colspan="2"| Total
| $1.90
|}
EOT;

    $t3 = <<<EOT
{| class="wikitable" style="max-width: 300px;"
|- style="vertical-align:top;"
| style="height:100px; width:100px; text-align:left;" | A
| style="height:100px; width:100px; text-align:center;" | B
| style="height:100px; width:100px; text-align:right;" | C
|- style="vertical-align:middle;"
| style="height:100px; width:100px; text-align:left;" | D
| style="height:100px; width:100px; text-align:center;" | E
| style="height:100px; width:100px; text-align:right;" | F
|- style="vertical-align:bottom;"
| style="height:100px; width:100px; text-align:left;" | G
| style="height:100px; width:100px; text-align:center;" | H
| style="height:100px; width:100px; text-align:right;" | I
|}
EOT;

    $t4 = <<<EOT
{| class="wikitable" style="margin: auto; max-width: 420px;"
| Orange
| Apple
|-
| Bread
| Pie
|-
| Butter
| Ice cream 
|}
EOT;

    $t5 = <<<EOT
{| class="wikitable" style="max-width: 480px;"
| Orange
| Apple
| style="text-align:right;"| 12,333.00
|-
| Bread
| Pie
| style="text-align:right;"| 500.00
|- style="font-style: italic; color: green;"
| Butter
| Ice cream
| style="text-align:right;"| 1.00
|}
EOT;

    $t6 = <<<EOT
{|
|Lorem ipsum dolor sit amet, 
consetetur sadipscing elitr, 
sed diam nonumy eirmod tempor invidunt
ut labore et dolore magna aliquyam erat, 
sed diam voluptua. 

At vero eos et accusam et justo duo dolores
et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est Lorem ipsum
dolor sit amet. 
|
* Lorem ipsum dolor sit amet
* consetetur sadipscing elitr
* sed diam nonumy eirmod tempor invidunt
|}
EOT;

    $tables = [$t1, $t2, $t3, $t4, $t5, $t6];
    for ($i = 0; $i <= 10; $i++) {
      $idx = mt_rand(0, count($tables) - 1);
      if (!in_array($idx, self::$table_done)) {
        break;
      }
    }

    self::$table_done[] = $idx;
    return "{$tables[$idx]}\n";
  }

}
