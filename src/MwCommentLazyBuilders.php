<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\MwCommentLazyBuilders.
 */
//namespace Drupal\comment;

namespace Drupal\sxt_mediawiki;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\comment\CommentInterface;
use Drupal\comment\CommentLazyBuilders;

/**
 * Defines a service for MediaWiki comment #lazy_builder callbacks.
 */
class MwCommentLazyBuilders extends CommentLazyBuilders {

  /**
   * {@inheritdoc}
   */
  public function renderForm($commented_entity_type_id, $commented_entity_id, $field_name, $comment_type_id) {
    $form = parent::renderForm($commented_entity_type_id, $commented_entity_id, $field_name, $comment_type_id);
    $form['#action'] = str_replace('/comment/reply/node/', '/mwcomment/reply/node/', $form['#action']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildLinks(CommentInterface $entity, EntityInterface $commented_entity) {
    $elements = parent::buildLinks($entity, $commented_entity);
    $reply_link = &$elements['#links']['comment-reply'] ?? FALSE;
    if ($reply_link) {
      $reply_link['url'] = Url::fromRoute('sxt_mediawiki.mwcomment.reply', [
            'entity_type' => $entity->getCommentedEntityTypeId(),
            'entity' => $entity->getCommentedEntityId(),
            'field_name' => $entity->getFieldName(),
            'pid' => $entity->id(),
      ]);
    }

    return $elements;
  }

}
