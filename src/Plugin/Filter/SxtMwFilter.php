<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Filter\SxtMwFilter.
 */

namespace Drupal\sxt_mediawiki\Plugin\Filter;

use Drupal\sxt_mediawiki\SxtMediawiki;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\sxt_mediawiki\SxtOAuth\SxtOAuthClient;
use Drupal\sxt_oauthclient\ClientConfig;
use Drupal\sxt_oauthclient\Consumer;
use Drupal\Core\Cache\Cache;

/**
 * Provides a filter for MediaWiki syntax usage.
 * 
 * @see https://www.mediawiki.org/wiki/API:Login/de
 * @see [my-mw]/Special:BotPasswords
 *
 * @Filter(
 *   id = "sxt_mediawiki",
 *   title = @Translation("MediaWiki syntax"),
 *   description = @Translation("Allows the conversion of content marked up using MediaWiki syntax."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *     "filter_mw_url" = "http://localhost/mediawiki",
 *     "filter_mw_ckey" = "-consumerKey-",
 *     "filter_mw_csecret" = "-consumerSecret-",
 *     "filter_mw_atoken" = "-accessToken-",
 *     "filter_mw_asecret" = "-accessSecret-",
 *     "filter_mw_rsapath" = "",
 *   }
 * )
 */
class SxtMwFilter extends FilterBase {

  protected static $instance = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    self::$instance = $this;
  }

  public static function getInstance() {
    if (!self::$instance) {
      $format = FilterFormat::load('mediawiki_format');
      $filters = $format->filters();  // this will create an instance
    }

    return self::$instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $mw_url = $this->settings['filter_mw_url'];
    $consumerKey = $this->settings['filter_mw_ckey'];
    $consumerSecret = $this->settings['filter_mw_csecret'];
    $accessToken = $this->settings['filter_mw_atoken'];
    $accessSecret = $this->settings['filter_mw_asecret'];
    $form['filter_mw_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $mw_url,
      '#description' => $this->t('URL of MediaWiki server; no "api.php/", no trailing slesh.'),
    ];
    $form['filter_mw_ckey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer key'),
      '#default_value' => $consumerKey,
      '#description' => $this->t('Consumer key as generated in your mediawiki server.'),
    ];
    $form['filter_mw_csecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer secret'),
      '#default_value' => $consumerSecret,
      '#description' => $this->t('Consumer secret as generated in your mediawiki server.'),
    ];

    $accessReady = (!empty($accessToken) && !empty($accessSecret));
    if (!empty($consumerKey) && !empty($consumerSecret) && !$accessReady) {
      $description = '';
      try {
        $url = $mw_url . '/index.php?title=Special:OAuth';
        $consumer = new Consumer($consumerKey, $consumerSecret);
        $conf = new ClientConfig($url, FALSE);
        $conf->setConsumer($consumer);
        $client = new SxtOAuthClient($conf, $this->logger());
        $client->setSettings($this->settings);
        list( $authorizeUrl, $requestToken ) = $client->initiate();
        $description = $this->t('NOTE: Use this url before token has expired:');
      }
      catch (\Exception $e) {
        $args = ['@message' => $e->getMessage()];
        $this->logger()->error(t('SettingsForm.SxtOAuthClient::initiate(): @message', $args));
        $authorizeUrl = '>>FEHLER: ' . $e->getMessage() . '<<';
      }
      $form['filter_mw_authorize'] = [
        '#type' => 'details',
        '#title' => $this->t('Authorize url'),
        '#description' => $description,
        '#open' => TRUE,
      ];
      $form['filter_mw_authorize']['url'] = [
        '#markup' => $authorizeUrl,
      ];
    }

    $form['filter_mw_atoken'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#default_value' => $accessToken,
      '#description' => $this->t('Access token as generated in your mediawiki server.'),
    ];
    $form['filter_mw_asecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access secret'),
      '#default_value' => $accessSecret,
      '#description' => $this->t('Access secret as generated in your mediawiki server.'),
    ];
    $description = $this->t('Absolute path of the rsa-sha1 files.')
        . ' (File names: cert.txt/key.txt)<br />'
        . $this->t('For security reasons set the path not reachable for users.');
    $form['filter_mw_rsapath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RSA-Sha1 path'),
      '#default_value' => $this->settings['filter_mw_rsapath'],
      '#description' => $description,
    ];
    return $form;
  }

  protected function _prepareForComment($html_text) {
    preg_match('|<nowiki>%%mwcomments::(\d+)::(\d+)::(\d+)%%</nowiki>|', $html_text, $match);
    if ($match) {
      $delimiter = $match[0];
      $parts = explode($delimiter, $html_text, 2);
      $ncotent = trim($parts[0]);
      // prepare comments only
      $pattern = '/[\r\n?][\r\n?]+/s';
      $comments = preg_replace($pattern, "$1\n\n$2", trim($parts[1]));
      $html_text = "$ncotent\n$delimiter\n\n$comments";
    }

    return $html_text;
  }

  protected function _processForComment($html_text, &$open_for_comments) {
    preg_match('|<p>\s*%%mwcomments::(\d+)::(\d+)::(\d+)%%\s*</p>|', $html_text, $match);
    if (empty($match)) {
      $open_for_comments = FALSE;
      return $html_text;
    }

    $nid = (integer) $match[1];
    $open_for_comments = (boolean) $match[2];
    $number_comments = (integer) $match[3];
    $many_comments = ($number_comments >= SlogXt::getMwCommentTocCompact());
    $has_comments = (boolean) $number_comments;
    $replace = '<div class="sxt-mwcomments" data-mwcomments-data="' . "$nid:$many_comments:$open_for_comments" . '">';
    $html_text = str_replace($match[0], $replace, $html_text);
    preg_match('|%%endmwcomments%%|', $html_text, $match);
    $html_text = str_replace($match[0], '</div>', $html_text);

    // comment items
    if ($has_comments) {
      preg_match_all('|<p>\s*%%mwcommentitem::(\d+)::(\d+)::(\S+)%%\s*</p>|', $html_text, $matches);
      $comments_found = count($matches[0]) ?? 0;
      if (empty($matches) || $comments_found !== $number_comments) {
        $args = [
          '@nid' => $nid,
          '@required' => $number_comments,
          '@found' => $comments_found,
        ];
        $this->logger()->error(t('Comments missmatch (nid/required/found): @nid/@required/@found', $args));
        return $html_text;
      }

      preg_match('|<p>\s*%%endmwcommentitem%%\s*</p>|', $html_text, $match);
      $html_text = str_replace($match[0], '</div>', $html_text, $count);
      preg_match('|<p><br />\s*%%endmwcommentitem%%\s*</p>|', $html_text, $match);
      if (!empty($match)) {
        $html_text = str_replace($match[0], '</div>', $html_text, $count);
      }
      foreach ($matches[0] as $idx => $search) {
        $cid = $matches[1][$idx];
        $depth = $matches[2][$idx];
        $actions = $matches[3][$idx];
        $cls = "sxt-mwcomment-item level-$depth";
        $replace = '<div class="' . $cls . '" data-mwcomment-cid="' . $cid . '" data-mwcomment-actions="' . $actions . '">';
        $html_text = str_replace($search, $replace, $html_text);
      }

      $prefix_head = '<div class="sxtmwc-actions-all mwc-all-head"><div class="sxtmwc-action sxtmwc-toggler-all">&nbsp;</div>';
      $prefix_foot = '<div class="sxtmwc-actions-all mwc-all-foot"><div class="sxtmwc-action sxtmwc-toggler-all">&nbsp;</div>';
      if ($open_for_comments) {
        $addtext = t('Add new comment');
        $replace = $prefix_head . '<div class="sxtmwc-action add-mwcomment-head">' . $addtext . '</div></div>';
        $html_text = str_replace('%%mwcommentshead%%', $replace, $html_text);
        $replace = $prefix_foot . '<div class="sxtmwc-action add-mwcomment-foot">' . $addtext . '</div></div>';
        $html_text = str_replace('%%mwcommentsfoot%%', $replace, $html_text);
      }
      else {
        $closedtext = t('Discussion is closed. No more comments can be added.');
        $replace = $prefix_head . '<div class="closed-mwcomment-head">' . $closedtext . '</div></div>';
        $html_text = str_replace('%%mwcommentshead%%', $replace, $html_text);
        $replace = $prefix_foot . '<div class="closed-mwcomment-foot">' . $closedtext . '</div></div>';
        $html_text = str_replace('%%mwcommentsfoot%%', $replace, $html_text);
      }

      // body
      preg_match('|(<p>%%mwcommentbody%%\s*</p>)|s', $html_text, $bmatch);
      preg_match('|(<p>%%endmwcommentbody%%\s*</p>)|s', $html_text, $bendmatch);
      $body_html_text = str_replace($bmatch[0], '<div class="sxt-mwcomment-body">', $html_text, $count_body);
      $body_html_text = str_replace($bendmatch[0], '</div>', $body_html_text, $count_endbody);
      if ($count_body === $count_endbody) {
        $html_text = $body_html_text;
      }
    }
    else {
      if ($open_for_comments) {
        $addtext = t('Add the first comment');
        $replace = '<div class="sxtmwc-action add-mwcomment-head">' . $addtext . '</div>';
        $html_text = str_replace('%%mwcommentshead%%', $replace, $html_text);
        $html_text = str_replace('%%mwcommentsfoot%%', '', $html_text);
      }
      else {
        $closedtext = t('Discussion is closed. No comments has been submitted.');
        $replace = '<div class="closed-mwcomment-head">' . $closedtext . '</div>';
        $html_text = str_replace('%%mwcommentsemptyclosed%%', $replace, $html_text);
      }
    }

    return $html_text;
  }

  /**
   * {@inheritdoc}
   */
  public static function getMwHeaderAnchor($header_text) {
    $test_text = "===$header_text===";
    $html_text = self::getInstance()->parseWikiText($test_text);
    preg_match('|<h3>.*id="(\S+)".*</h3>|', $html_text, $match);

    return $match[1] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function process($mw_text, $langcode) {
    $request = \Drupal::request();
    $cid = SxtMediawiki::getCidFromText($mw_text);
    if (!$request->get('deeprefresh') && $cache = SxtMediawiki::cache()->get($cid)) {
      $html_text = $cache->data;
    }
    else {
      $html_text = $this->_prepareForComment($mw_text);
      $html_text = $this->parseWikiText($html_text);
      $html_text = $this->_processForComment($html_text, $open_for_comments);
      if (!$request->get('nodeEditPreview')) {
        $expire = $open_for_comments ? REQUEST_TIME + 86400 : Cache::PERMANENT;
        SxtMediawiki::cache()->set($cid, $html_text, $expire);
      }
    }

    return new FilterProcessResult($html_text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $args = [':url' => 'https://www.mediawiki.org/wiki/Help:Formatting'];
    return $this->t('Write your content in MediaWiki syntax. Formatting help you get <a href=":url" target="_blank">here</a>.', $args);
  }

  /**
   * Call to mediawiki server for parsing text, calling by OAuth.
   * 
   * @param string $text 
   *  The mediawiki text to be parsed
   * @return string
   *  By MediaWiki server parsed text or unchanged text on error.
   */
  protected function parseWikiText($text) {
    try {
      $consumerKey = $this->settings['filter_mw_ckey'];
      $consumerSecret = $this->settings['filter_mw_csecret'];
      $consumer = new Consumer($consumerKey, $consumerSecret);

      $url = $this->settings['filter_mw_url'] . '/index.php?title=Special:OAuth';
      $conf = new ClientConfig($url, FALSE);
      $conf->setConsumer($consumer);

      $client = new SxtOAuthClient($conf, $this->logger());
      $client->setSettings($this->settings);

      return $client->apiParseText($text);
    }
    catch (\Exception $e) {
      $args = ['@message' => $e->getMessage()];
      $this->logger()->error(t('Error on parsing wiki text: @message', $args));
    }

    return $text;
  }

  /**
   * Logger with channel equals to 'sxt_mediawiki'.
   * 
   * @return \Psr\Log\LoggerInterface
   *   The logger for channel = 'sxt_mediawiki'.
   */
  protected function logger() {
    return SxtMediawiki::logger();
  }

}
