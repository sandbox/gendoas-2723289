<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldWidget\MediaWikiWidget.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldWidget;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

//use Drupal\field\Entity\FieldConfig;

/**
 * Plugin implementation of the 'text_textarea_with_summary' widget.
 *
 * @FieldWidget(
 *   id = "sxt_mediawiki",
 *   label = @Translation("MediaWiki"),
 *   description = @Translation("...............MediaWiki Widget."),
 *   field_types = {
 *     "sxt_mediawiki",
 *     "sxt_mwcomment",
 *   }
 * )
 */
class MediaWikiWidget extends TextareaWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rows' => '9',
      'placeholder' => '',
        ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_definition = $items->getFieldDefinition();
    $is_default_value_widget = (bool) $form_state->get('default_value_widget');
    if (!$is_default_value_widget) {
      $user = \Drupal::currentUser();
      $formats = filter_formats($user);
      $field_configuration = $field_definition->getConfig($field_definition->getTargetBundle());

      $allowed_format = $field_configuration->getThirdPartySetting('sxt_mediawiki', 'allowed_format');
      if (in_array($allowed_format, array_keys($formats))) {
        $disabled = FALSE;
      }
      else {
        $disabled = TRUE;
        $config = \Drupal::config('filter.settings');
        $allowed_format = $config->get('fallback_format');
      }

      $element['#format'] = $allowed_format;
      $element['#allowed_formats'] = [$allowed_format];
      $element['#disabled'] = $disabled;
    }

    return $element;
  }

}
