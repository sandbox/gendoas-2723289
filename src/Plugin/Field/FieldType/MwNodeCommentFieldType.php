<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldType\MwNodeCommentFieldType.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\comment\Plugin\Field\FieldType\CommentItem;

/**
 * Plugin implementation of the 'sxt_mediawiki' field type.
 *
 * @FieldType(
 *   id = "mwnodecomment_fieldtype",
 *   label = @Translation("MW Node Comment"),
 *   description = @Translation("...............MW Node Comment Field Type."),
 *   list_class = "\Drupal\comment\CommentFieldItemList",
 *   default_widget = "mwnodecomment_widget",
 *   default_formatter = "mwnodecomment_formatter",
 *   cardinality = 1,
 * )
 */
class MwNodeCommentFieldType extends CommentItem {

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'status' => CommentItemInterface::OPEN,
    ];
  }

}
