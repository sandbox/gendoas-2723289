<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\SxtMediawiki.
 */

namespace Drupal\sxt_mediawiki;

use Drupal\sxt_mediawiki\Plugin\Filter\SxtMwFilter;
use Drupal\node\NodeInterface;
use Drupal\Core\Render\Element;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\FormStateInterface;

/**
 * Static functions for sxt_mediawiki module
 */
class SxtMediawiki {

  protected static $cache;
  protected static $original = [];

  /**
   * Return cache object for table 'cache_sxt_mediawiki'
   *
   * @return Drupal\Core\Cache\CacheBackendInterface 
   */
  public static function cache() {
    if (!self::$cache) {
      self::$cache = \Drupal::cache('sxt_mediawiki');
    }
    return self::$cache;
  }

  public static function getCidFromText($text) {
    $text = str_replace("\r", "", $text);
    return Crypt::hashBase64($text);
  }

  public static function getMwCommentManager() {
    return \Drupal::service('sxt_mediawiki.mwnodecomment.manager');
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  public static function configEditFormAlter(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Field\FieldConfigInterface $field */
    $field = $form_state->getFormObject()->getEntity();
    $alter = [
      'sxt_mediawiki' => 'mediawiki_format',
      'sxt_mwcomment' => 'mediawiki_comment_format',
    ];
    $alter_keys = array_keys($alter);
    $field_type = $field->getType();
    if (in_array($field_type, $alter_keys)) {
      $options = [];
      $format_setting = $alter[$field_type];
      foreach (filter_formats() as $format) {
        $format_id = $format->id();
        if ($format_id === $format_setting) {
          $options[$format_id] = $format->label();
        }
      }

      $form['third_party_settings']['sxt_mediawiki']['allowed_format'] = [
        '#type' => 'select',
        '#title' => t('Use this MediaWiki format'),
        '#options' => $options,
        '#default_value' => $field->getThirdPartySetting('sxt_mediawiki', 'allowed_format'),
        '#description' => t('Ensure you selected a format with the MediaWiki filter.')
      ];
    }
  }

  /**
   * Make original node values available.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function nodeFormAlter(&$form, FormStateInterface $form_state) {
    $node = $form_state->getFormObject()->getEntity();
    $nid = $node->id();
    if (empty(self::$original[$nid])) {
      self::$original[$nid] = $node;
    }
  }

  /**
   * Make changes for mwcomment field.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function nodeGenerateFormAlter(&$form, FormStateInterface $form_state) {
    if (!empty($form['node_types']['#options'])) {
      $options = &$form['node_types']['#options'];
      $type_ids = Element::children($options);
      if (!empty($type_ids)) {
        $field_manager = \Drupal::service('entity_field.manager');
        foreach ($type_ids as $type_id) {
          $field_definitions = array_filter($field_manager->getFieldDefinitions('node', $type_id), function ($field_definition) {
            return $field_definition instanceof FieldConfigInterface;
          });

          if (!empty($field_definitions['mwnodecomment'])) {
            $comment_fields = self::getMwCommentManager()->getFields('node');
            $map = [t('Hidden'), t('Closed'), t('Open')];

            $fields = [];
            foreach ($comment_fields as $field_name => $info) {
              // Find all comment fields for the bundle.
              if (in_array($type_id, $info['bundles'])) {
                $instance = FieldConfig::loadByName('node', $type_id, $field_name);
                $default_value = $instance->getDefaultValueLiteral();
                $default_mode = reset($default_value);
                $fields[] = new FormattableMarkup('@field: @state', [
                  '@field' => $instance->label(),
                  '@state' => (string) $map[$default_mode['status']],
                ]);
              }
            }
            
            // @todo Refactor display of comment fields.
            if (!empty($fields)) {
              $options[$type_id]['comments'] = [
                'data' => [
                  '#theme' => 'item_list',
                  '#items' => $fields,
                ],
              ];
            }
          }
        }
      }
    }
  }

  /**
   * Delete cache if contents of mwdiawiki fields changed.
   * 
   * @param NodeInterface $node
   */
  public static function nodeFormUpdate(NodeInterface $node) {
    $nid = $node->id();
    $orig_node = isset(self::$original[$nid]) ? self::$original[$nid] : FALSE;
    if ($orig_node && self::nodeHasMwFields($orig_node)) {
      $mw_items_orig = self::nodeGetMwItems($orig_node);
      $mw_items_new = self::nodeGetMwItems($node);
      foreach ($mw_items_orig as $key => $item_orig) {
        $item_new = $mw_items_new[$key];
        foreach ($item_orig->getValue() as $delta => $value) {
          $content = $value['value'];
          $cid_orig = self::getCidFromText($content);
          $content_new = $item_new->get($delta)->value;
          $cid_new = self::getCidFromText($content_new);

          if ($cid_new !== $cid_orig) {
            self::cache()->delete($cid_orig);
          }
        }
      }
    }
  }

  /**
   * Delete cache of each mwdiawiki fields of deleted content.
   * 
   * @param NodeInterface $node
   */
  public static function nodeOnDelete(NodeInterface $node) {
    if (self::nodeHasMwFields($node)) {
      $mw_items = self::nodeGetMwItems($node);
      foreach ($mw_items as $item) {
        foreach ($item->getValue() as $value) {
          $cid = self::getCidFromText($value['value']);
          self::cache()->delete($cid);
        }
      }
    }
  }

  public static function nodeOnInsert(NodeInterface $node) {
    if (!isset($node->devel_generate)) {
      return;
    }

    $results = $node->devel_generate;
    if (!empty($results['max_comments'])) {
      foreach ($node->getFieldDefinitions() as $field_name => $field_definition) {
        if ($field_definition->getType() == 'mwnodecomment_fieldtype') {
          // Add comments for each comment field on entity.
          devel_generate_add_comments($node, $field_definition, $results['users'], $results['max_comments'], $results['title_length']);
        }
      }
    }

    // Add an url alias. Cannot happen before save because we don't know the nid.
    if (!empty($results['add_alias'])) {
      $path = array(
        'source' => '/node/' . $node->id(),
        'alias' => '/node-' . $node->id() . '-' . $node->bundle(),
      );
      \Drupal::service('path.alias_storage')->save($path['source'], $path['alias']);
    }

    // Add node statistics.
    if (!empty($results['add_statistics']) && \Drupal::moduleHandler()->moduleExists('statistics')) {
      devel_generate_add_statistics($node);
    }
  }

  /**
   * Whether node has mediawiki fields.
   * 
   * @param NodeInterface $node
   * @return boolean
   */
  public static function nodeHasMwFields(NodeInterface $node) {
    $mw_items = self::nodeGetMwItems($node);
    return !empty($mw_items);
  }

  /**
   * Return all mediawiki field's FieldItemList items of the node.
   * 
   * @param NodeInterface $node
   * @return array of \Drupal\Core\Field\FieldItemList items
   */
  public static function nodeGetMwItems(NodeInterface $node) {
    $mw_items = [];
    foreach ($node->getFieldDefinitions() as $name => $field) {
      if ($field->getType() === 'sxt_mediawiki') {
        $mw_items[$name] = $node->get($name);
      }
    }

    return $mw_items;
  }

  public static function replaceMwHeader($mw_text) {
    $rdata = [
      '/(^======[^=]\s*.*\S\s*======)/' => "<nowiki>\${1}</nowiki>",
      '/(^=====[^=]\s*.*\S\s*=====)/' => "<nowiki>\${1}</nowiki>",
      '/(^====[^=]\s*.*\S\s*====)/' => "<nowiki>\${1}</nowiki>",
      '/(^===[^=]\s*.*\S\s*===)/' => "<nowiki>\${1}</nowiki>",
      '/(^==[^=]\s*.*\S\s*==)/' => "<nowiki>\${1}</nowiki>",
      '/(^=[^=]\s*.*\S\s*=)/' => "<nowiki>\${1}</nowiki>",
    ];
    $search = array_keys($rdata);
    $replace = array_values($rdata);

    $splitted = explode(PHP_EOL, $mw_text);
    foreach ($splitted as &$line) {
      $line = preg_replace($search, $replace, $line);
    }

    $new_text = implode(PHP_EOL, $splitted);
    return $new_text;
  }

  /**
   * {@inheritdoc}
   */
  public static function getMwHeaderAnchor($header_text) {
    return SxtMwFilter::getMwHeaderAnchor($header_text);
  }

  public static function logger() {
    return \Drupal::logger('sxt_mediawiki');
  }

}
