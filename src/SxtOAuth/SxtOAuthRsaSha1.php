<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\SxtOAuth\SxtOAuthRsaSha1.
 */

namespace Drupal\sxt_mediawiki\SxtOAuth;

use Drupal\sxt_oauthclient\SignatureMethod\RsaSha1;
use Drupal\sxt_oauthclient\Request;

/**
 * {@inheritdoc}
 */
class SxtOAuthRsaSha1 extends RsaSha1 {

  /**
   * The path containing the rsa files.
   * 
   * This path should be unreachable for users for security reasons.
   *
   * @var array 
   */
  protected $rsa_path;

  /**
   *
   * @var \Psr\Log\LoggerInterface 
   */
  public $logger;

  /**
   * @param string $rsa_path 
   */
  function __construct($rsa_path) {
    $this->rsa_path = !empty($rsa_path) ? $rsa_path : __DIR__ . '/rsa-sha1';
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchPublicCert(Request $request) {
    return $this->_getCert('/cert.txt');
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchPrivateCert(Request $request) {
    return $this->_getCert('/key.txt');
  }

  protected function _getCert($cert_file) {
    $cert = '';
    $file = $this->rsa_path . $cert_file;
    if (file_exists($file)) {
      $cert = file_get_contents($file);
      if (empty($cert)) {
        $this->logger->error('No content found in file: ' . $file);
      }
    } else {
      $this->logger->error('File not found: ' . $file);
    }
    return $cert;
  }

}
