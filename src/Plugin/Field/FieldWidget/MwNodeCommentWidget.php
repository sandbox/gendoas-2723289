<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldWidget\MwNodeCommentWidget.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldWidget;

use Drupal\comment\Plugin\Field\FieldWidget\CommentWidget;

/**
 * Provides a default MediaWiki comment widget.
 *
 * @FieldWidget(
 *   id = "mwnodecomment_widget",
 *   label = @Translation("MW Node Comment"),
 *   description = @Translation("...............MW Node Comment Widget."),
 *   field_types = {
 *     "mwnodecomment_fieldtype"
 *   }
 * )
 */
class MwNodeCommentWidget extends CommentWidget {

  
}
