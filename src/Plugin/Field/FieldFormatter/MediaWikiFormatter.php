<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldFormatter\MediaWikiFormatter.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldFormatter;

use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of the 'sxt_mediawiki' formatter.
 *
 * @FieldFormatter(
 *   id = "sxt_mediawiki",
 *   label = @Translation("MediaWiki"),
 *   description = @Translation("...............MediaWiki Formatter."),
 *   field_types = {
 *     "sxt_mediawiki",
 *     "sxt_mwcomment",
 *   }
 * )
 */
class MediaWikiFormatter extends TextDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $elements['#attached']['library'][] = 'sxt_mediawiki/sxt_mediawiki';
    
    $format_id = $items->getFieldDefinition()->getThirdPartySetting('sxt_mediawiki', 'allowed_format');
    foreach (Element::children($elements) as $key) {
      $elements[$key]['#format'] = $format_id;
    }

    return $elements;
  }

}
