<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldType\MwCommentFieldType.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldType;

use Drupal\sxt_mediawiki\Plugin\Field\FieldType\MediaWikiFieldType;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'sxt_mwcomment' field type.
 *
 * @FieldType(
 *   id = "sxt_mwcomment",
 *   label = @Translation("MW Comment"),
 *   description = @Translation("This field stores comments in mediawiki format."),
 *   category = @Translation("Text"),
 *   default_widget = "sxt_mediawiki",
 *   default_formatter = "sxt_mediawiki"
 * )
 */
class MwCommentFieldType extends MediaWikiFieldType {

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = self::generateParagraphs(mt_rand(1, 4), self::PARA_TEXT);
    return $values;
  }

  protected static function getParagraphsAvailable() {
    return [
      self::PARA_TEXT,
      self::PARA_ULIST,
      self::PARA_ULIST,
      self::PARA_OLIST,
      self::PARA_OLIST,
      self::PARA_DEFLIST,
    ];
  }

}
