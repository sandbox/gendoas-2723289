<?php

/**
 * @file
 * Contains \Drupal\sxt_mediawiki\Plugin\Field\FieldFormatter\MwNodeCommentFormatter.
 */

namespace Drupal\sxt_mediawiki\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\comment\Plugin\Field\FieldFormatter\CommentDefaultFormatter;

/**
 * Provides a MediaWiki comment formatter.
 *
 * @FieldFormatter(
 *   id = "mwnodecomment_formatter",
 *   module = "sxt_mediawiki",
 *   label = @Translation("MW Node Comment"),
 *   description = @Translation("...............MW Node Comment Formatter."),
 *   field_types = {
 *     "mwnodecomment_fieldtype"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class MwNodeCommentFormatter extends CommentDefaultFormatter {

  public static function preRenderComments($elements) {
    foreach ($elements as $key => &$element) {
      if ((integer)$key > 0 && !empty($element['links'])) {
        $element['links']['#lazy_builder'][0] = 'sxt_mediawiki.mwcomment.lazy_builders:renderLinks';
      }      
    }
    
    
    
    return $elements;
  }
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    if (!empty($elements)) {
      $comments = & $elements[0]['comments'];
      $comments['#pre_render'][] = [get_class($this), 'preRenderComments'];

      $field_name = $this->fieldDefinition->getName();
      $entity = $items->getEntity();

      $lazy_builder = [
        'sxt_mediawiki.mwcomment.lazy_builders:renderForm',
        [
          $entity->getEntityTypeId(),
          $entity->id(),
          $field_name,
          $this->getFieldSetting('comment_type'),
        ],
      ];
      $elements[0]['comment_form']['#lazy_builder'] = $lazy_builder;
    }

    return $elements;
  }

}
